# Business Context Definition

## Name
"Business Context Definition" is the foundational project in the Organization Building Value Stream of Virtual Agile Teams Ltd.  All activities and tasks related to createing and bootstrapping Virtual Agile Teams Ltd. business will be collected, recorded and managed in this project.

## Description
Business Context of Virtual Agile Teams Ltd. defines all subsquent Value Streams of this Co-Operative.  Value Streams are the main organizational constructs necessary to align Partners' efforts to build and manage the Co-Operative.  Business Context is necessary to define and describe Objectives - Key Results (OKRs), implemented in the format of the Strategic Themes that will be explored and defined for implementation via a Portfolio of Programs (or Projects, or Products), in order to reach the Objectives and thus satisfy the needs of such Value Streaam.  

## Installation
This project is for collection of knowledge, tracking down ideas and making decisions.  It will not be deployed, unless the contents of this project will be deemed appropriate for publication on the Virtual Agile Teams Ltd. public web site.  

## Usage
This Business Context Definition project will have a lifetime of the Virtual Agile Teams Ltd.  It is the first project created and will continue until Virtual Agile Teams Ltd. is dissolved or terminated.  All Value Streams, Strategic Themes and Portfolios of the Co-Operative will be initially defined here. 

## Support
For the initial support with this project, please contact the owner of the project at:  alex@glodigapp.io.

## Roadmap
This project has a dynamic nature.  It will define and describe on high level all Value Streams of the Co-Operative.  The project will track all active Value Streams.  Specific design and implementation details of the defined Value Streams will be reflected in separate projects, created for such Value Streams.  Once a Value Stream is deemed no longer necessary, it will be removed from this project, and its' specific implementation project will be archived from this platform. 

## Contributing
Intially, the Founding Partners will be contributing to this project.  As the Co-Operative grows, all Partners will be able to contribute to the project via pull requests. 

## Authors and acknowledgment
This project is originally created by [Alexander Mitchen](alex@glodigapp.io).

## License
This is a Copyleft GNU AGPL licensed project.

## Project status
This project is active and open for contributions. 
