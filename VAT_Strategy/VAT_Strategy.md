Virage Agile Teams Ltd. Strategey Statement
(VAT Strategy)

"Strategy is a tool to achieve one or more goals under conditions of uncertainty" 
(O'Reilly Live, Product Managers Strategy Workshop)

Strategy involves the following:

1. Prioritize problems in the competitive environment to be solved.

For VAT, such problems are:

*

* 


2. To clarify long-term goals in terms of solving specific problems. 

For VAT, such clarification of goals are:

*

* 


3. To define actions that are necessary to realize the Strategy.

For VAT, such actions are defined in the form of the Value Streams (as per SAFe method), and they are:

* 

* 

*