Mission of Virtual Agile Teams Ltd.
(VAT Mission Statement)

Virtual Agile Teams Ltd. is created by and for Digital Knoweledge Workers who are challenged and motivated to address the growing technocracy tendencies in the modern corporate and enterprise environment where high productivity of outcomes and the increasing value created by people's work does not proportionally translate into benefits for those who create such results.   

The VAT Mission is NOT to change the existing economic model or Capital-Labor relations in the traditional terms of "social experiments", like revolutions, trade union movement or any other method.  The VAT Mission is to create an ALTERNATIVE to the existing form of economic and labor organization in which all people doing the work have equal rights in making decisions about the products they create.  Moreover, VAT is to enable people to organize into teams of Partners (or VAT Members) to collectively create value and share in the results of their work, not as wage workers (aka "wage slaves") but as business partners receiving Patronage Distributions from the proceeds of the business. 

VAT Mission is aligned with VAT Vision, that is to become the best place for Digitial Knowledge Workers to come together to create value for themselves (as the wealth that they create), as well as for their clients, which is the society at large, since it is expected that the Digital Knowledge Workers will be motivated to create meaninful and needed products.  With no other Stakeholders in the business other than themselves, VAT Members will be more inclined to apply their solutions to creating products that improve their personal lives, as well as the lives of their families, friends and their communities.  

