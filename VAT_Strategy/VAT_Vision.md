Vision for Virtual Agile Teams Ltd. 
(VAT Vision)

Our Vision for Virtual Agile Teams Ltd. is to create an organization for its Members and Partners to prosper and create wealth through work and collaboration resulting in value creation with fair and contributions-based equity allocation in all proceeds and surplus from revenues received.

We want to create a brand and a model for Digital Knowledge Workes to collaborate with trust, transparency, agility and productivity surpassing the big name companies in the Digital Economy business as the best place to work for, as voted by people who do the work and create value.  

We want to enable creative and enterpreneurial people to transition to a new model of work, in which they act not as employees (aka "wage slaves"), but as equal rights Partners in the common business and enterprise.  In this model, they collaborate in teams organized on the Collective or Co-Operative principles.  

We want to enable our Members and Partners to pay taxes not as employees but as "business owners" from the revenues recieved from the results of their work, not from selling their time worked.