# Time-Tracking Application

Time-tracking is the most important responsibility of every Member-Partner of Virtual Agile (VirAge) Teams Ltd. Collective.  

Time should be recorded in 12 minute increments, 6 increments per 1 hour. 

It is expected that "One work hour" is equal to "48 minutes of actual work" with one 12-minute increment. 