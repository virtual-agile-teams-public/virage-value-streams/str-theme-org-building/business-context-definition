# OKR: Subscriptin to VirAge Mailing List

VirAge Mailing List Subsciption is the tool to inform interested individuals about VirAge activities, news, publications and any other public (not private or interenal) information about VirAge affairs.

| Objective | Key Results |
| --------- | ----------- |
| All people interested in VirAge Teams should be able to subscribe to the Mailing List via web or mobile to learn about VirAge events. 
| Subcription is available to all site visitors, and user-provided information is stored in a secure database used only to send notifications to learn about VirAge events.|
